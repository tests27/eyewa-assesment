import { ChakraProvider } from "@chakra-ui/react";
import { Provider as ReduxProvider } from "react-redux";
import { setupAxios } from "shared/api";
import { HomePage } from "pages/home-page";
import { store } from "./store";

setupAxios();

function App() {
  return (
    <ChakraProvider>
      <ReduxProvider store={store}>
        <HomePage />
      </ReduxProvider>
    </ChakraProvider>
  );
}

export default App;
