import { configureStore } from "@reduxjs/toolkit";
import { apiSlice } from "shared/api";

export const store = configureStore({
  reducer: {
    [apiSlice.reducerPath]: apiSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(apiSlice.middleware),
});

export type RootStateType = typeof store.getState;
export type AppDispatch = typeof store.dispatch;
