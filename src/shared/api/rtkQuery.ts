import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Config } from "shared/config";

export const apiSlice = createApi({
  reducerPath: "api",

  baseQuery: fetchBaseQuery({
    baseUrl: Config.starWarsApiUrl,
  }),

  endpoints: () => ({}),
});
