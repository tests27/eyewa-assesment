import axios from "axios";
import { Config } from "shared/config";

const axiosInstance = axios.create({
  baseURL: Config.starWarsApiUrl,
});

export const setupAxios = () => {
  // setup axios with interceptors
  // axiosInstance.interceptors.request.use();
};

export default axiosInstance;
