import {
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputRightAddon,
} from "@chakra-ui/react";
import { BiSearchAlt } from "react-icons/bi";

interface SearchInputProps {
  value: string;
  onChange: (value: string) => void;
}

export const SearchInput: React.FC<SearchInputProps> = ({
  value,
  onChange,
}) => {
  return (
    <FormControl>
      <FormLabel>Search character</FormLabel>
      <InputGroup>
        <Input value={value} onChange={(e) => onChange(e.target.value)} />
        <InputRightAddon>
          <BiSearchAlt />
        </InputRightAddon>
      </InputGroup>
    </FormControl>
  );
};
