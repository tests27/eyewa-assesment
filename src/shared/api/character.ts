import axiosInstance from "./axios";

export interface CharacterObject {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
  species: string[];
  vehicles: string[];
  starships: string[];
  created: string;
  edited: string;
  url: string;
}

export interface PeopleSearchRequest {
  searchText: string;
}

export interface PeopleSearchResponse {
  count: number;
  next: null;
  previous: null;
  results: CharacterObject[];
}

export const searchPeople = async (req: PeopleSearchRequest) => {
  const res = await axiosInstance.get<PeopleSearchResponse>(
    `/people/?search=${req.searchText}`
  );

  return res;
};
