import {
  Flex,
  Stack,
  TableContainer,
  Table,
  Tbody,
  Tr,
  Td,
} from "@chakra-ui/react";
import { Character } from "entities/character";
import { useState } from "react";
import { SearchBar } from "widgets/search-bar";

export const HomePage = () => {
  const [selectedCharacter, setSelectedCharacter] =
    useState<Character.CharacterInfo | null>(null);

  return (
    <Flex maxWidth="800px" padding="24px" gap="24px">
      <SearchBar onSelect={setSelectedCharacter} styledProps={{ flex: 1 }} />

      <Stack flex="2" gap="16px" backgroundColor="gray.100">
        {selectedCharacter && (
          <TableContainer>
            <Table variant="simple">
              <Tbody>
                <Tr>
                  <Td>name</Td>
                  <Td>{selectedCharacter.name}</Td>
                </Tr>

                <Tr>
                  <Td>height</Td>
                  <Td>{selectedCharacter.height}</Td>
                </Tr>

                <Tr>
                  <Td>mass</Td>
                  <Td>{selectedCharacter.mass}</Td>
                </Tr>

                <Tr>
                  <Td>hair color</Td>
                  <Td>{selectedCharacter.hairColor}</Td>
                </Tr>

                <Tr>
                  <Td>skin color</Td>
                  <Td>{selectedCharacter.skinColor}</Td>
                </Tr>

                <Tr>
                  <Td>eye color</Td>
                  <Td>{selectedCharacter.eyeColor}</Td>
                </Tr>

                <Tr>
                  <Td>birth year</Td>
                  <Td>{selectedCharacter.birthYear}</Td>
                </Tr>

                <Tr>
                  <Td>gender</Td>
                  <Td>{selectedCharacter.gender}</Td>
                </Tr>
              </Tbody>
            </Table>
          </TableContainer>
        )}
      </Stack>
    </Flex>
  );
};
