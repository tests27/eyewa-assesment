import { charactersApi } from "shared/api";
import { CharacterInfo } from "./model";

export const mapCharacter = (
  character: charactersApi.CharacterObject
): CharacterInfo => {
  return {
    name: character.name,
    height: character.height,
    mass: character.mass,
    hairColor: character.hair_color,
    skinColor: character.skin_color,
    eyeColor: character.eye_color,
    birthYear: character.birth_year,
    gender: character.gender,
    homeworld: character.homeworld,
    films: character.films,
    species: character.species,
    vehicles: character.vehicles,
    starships: character.starships,
    created: character.created,
    edited: character.edited,
    url: character.url,
  };
};
