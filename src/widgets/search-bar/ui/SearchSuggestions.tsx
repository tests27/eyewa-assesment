import { Button, Center, Spinner, Stack, StackProps } from "@chakra-ui/react";
import { Character } from "entities/character";

interface SearchSuggestionsProps {
  isLoading?: boolean;
  characters?: Character.CharacterInfo[];
  onSelect: (character: Character.CharacterInfo) => void;
  styledProps?: StackProps;
}

export const SearchSuggestions: React.FC<SearchSuggestionsProps> = ({
  characters,
  isLoading,
  onSelect,
}) => {
  return (
    <Stack gap="8px" background="gray.100" padding="8px">
      {isLoading && (
        <Center>
          <Spinner />
        </Center>
      )}
      {characters?.map((character) => (
        <Button
          variant="ghost"
          colorScheme="green"
          key={character.url}
          onClick={() => onSelect(character)}
        >
          {character.name}
        </Button>
      ))}
    </Stack>
  );
};
