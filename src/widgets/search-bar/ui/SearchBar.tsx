import { useCallback, useEffect, useMemo, useState } from "react";
import { SearchInput } from "./SearchInput";
import { SearchSuggestions } from "./SearchSuggestions";
import { Character, useLazySearchPeopleQuery } from "entities/character";
import { debounce } from "lodash";
import { Box, StackProps, useToast } from "@chakra-ui/react";

interface SearchBarProps {
  onSelect: (character: Character.CharacterInfo) => void;
  styledProps?: StackProps;
}

export const SearchBar: React.FC<SearchBarProps> = ({ onSelect }) => {
  const toast = useToast();
  const [searchText, setSearchText] = useState("");
  const [search, { isFetching, data }] = useLazySearchPeopleQuery();
  const foundCharacters = useMemo(
    () => (isFetching ? [] : data),
    [isFetching, data]
  );

  const [showSuggestions, setShowSuggestions] = useState(false);

  const debouncedSearch = useCallback(
    debounce(async (text: string) => {
      try {
        await search({
          searchText: text,
        }).unwrap();
      } catch {
        toast({
          status: "error",
          description: "Something bad happened",
          duration: 3000,
          isClosable: true,
        });
      }
    }, 300),
    []
  );

  useEffect(() => {
    if (searchText.length >= 2) {
      setShowSuggestions(true);
      debouncedSearch(searchText);
    }
  }, [searchText]);

  const onCharacterSelect = (character: Character.CharacterInfo) => {
    onSelect(character);
    setShowSuggestions(false);
  };

  return (
    <Box position="relative">
      <SearchInput value={searchText} onChange={setSearchText} />

      {showSuggestions && (
        <SearchSuggestions
          isLoading={isFetching}
          characters={foundCharacters}
          onSelect={onCharacterSelect}
        />
      )}
    </Box>
  );
};
