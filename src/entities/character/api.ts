import { apiSlice, charactersApi } from "shared/api";
import { mapCharacter } from "./lib";
import { CharacterInfo, SearchCharactersInput } from "./model";

const endpoints = apiSlice
  .enhanceEndpoints({
    addTagTypes: ["Character"],
  })
  .injectEndpoints({
    endpoints: (builder) => ({
      searchPeople: builder.query<CharacterInfo[], SearchCharactersInput>({
        queryFn: async (args) => {
          const res = await charactersApi.searchPeople(args);

          const characters = res.data.results.map(mapCharacter);

          return {
            data: characters,
          };
        },

        providesTags: (characters = []) => [
          "Character",
          ...characters.map((character) => ({
            type: "Character" as const,
            id: character.url,
          })),
        ],
      }),
    }),
  });

export const { useLazySearchPeopleQuery } = endpoints;
